# lpack-bash
Bash script that creates a .lbry archive that supports JS and CSS within the LBRY app! The original script was in fish. This is a conversion and improvement over the original one.

Inspired from [https://open.lbry.com/blog#4](https://open.lbry.com/blog#4) and [https://open.lbry.com/@BrendonBrewer:3/websites:0](https://open.lbry.com/@BrendonBrewer:3/websites:0).

## Requirements

- Linux
- bash
- zstd
- tar

## Testing

There is a test file on this repo to test basic functionality. Navigate to `test` directory README.md file for details. See [here](https://open.lbry.com/zstdlbrytest:6) for a test upload to see it in action!

## Usage

Make sure it is executable: `chmod +x lpack.bash`

To make it easily accessible from any directory, keep it on a directory in the `$PATH`. e.g. `sudo cp lpack.bash /usr/bin/`

Run:

```
$ lpack.bash /path/to/your/html/files yourfilename.lbry
```

This should create a `yourfilename.lbry` file ready to be uploaded to LBRY.


## Alternatives

There is an official project called [LBRY format](https://github.com/lbryio/lbry-format) that you might want to check. It's based on Node.js.


**License: MIT**
