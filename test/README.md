# Test lpack.bash

This directory contains a test HTML with basic JS and CSS to test if this works for your machine.

Just run on terminal:

```
../lpack.bash lbry-test test.lbry
```

Now check if there is a `test.lbry` file in this directory. If there is, the script is working. You can upload the .lbry file to LBRY to check if it is working for you.

If you want to see this file in LBRY, check this `lbry://zstdlbrytest#6` or [https://open.lbry.com/zstdlbrytest:6](https://open.lbry.com/zstdlbrytest:6).